### 8 Setting up the environment
#### 8.1 Sourcing OpenFOAM
OpenFOAM makes use of plenty of environment variables, see Section 11.1.1 for a brief discussion. In order to use OpenFOAM, we need to assign values to the variables. Another task enabling the convenient use of OpenFOAM is to add the directories in which OpenFOAM executables are located to the system’s $PATH variable.
The name of this section stems from the Linux command source, which is used in setting up the proper environment for using OpenFOAM. Setting up the environment for using OpenFOAM can be done in two ways, which are discussed below. Each of these variants involves editing a .bashrc file14. This .bashrc file can be either a systemwide one for systemwide installations, or belonging to the user who installed OpenFOAM in his/her home directory.
Once the OpenFOAM environment has been sourced in a Terminal, OpenFOAM is ready to use as long as the Terminal is open.

##### 8.1.1 Permanently sourcing OpenFOAM
If we only use one OpenFOAM installation, we could permanently source OpenFOAM. In this case, once this is set up, OpenFOAM is ready for use without any further user action. To achieve this, we add the following line to the appropriate .bashrc file. In the case of a single user’s installation, this would be the file $HOME/.bashrc.The $HOME/.bashrc file is loaded every time a Terminal is opened. Thus, if we add the command of Listing 20 to the $HOME/.bashrc file, then OpenFOAM is ready to use, whenever the user opens a Terminal. This also applies to login shells, thus remote connections via SSH or systems without any graphical desktop are covered as well.
```
source $HOME / OpenFOAM / OpenFOAM -4.0/ etc / bashrc
```
Listing 20: Permanently sourcing OpenFOAM

##### 8.1.2 Sourcing OpenFOAM on demand
Permanently sourcing OpenFOAM is impossible if we want to use several OpenFOAM versions alongside each other. If we have OpenFOAM-3.0 and OpenFOAM-4.1 installed on our system, where should/does $FOAM_SRC point to?
In this case, we need a solution to set up the OpenFOAM environment on demand for a specific version of OpenFOAM. Again, we need to add instructions to the .bashrc file. However, now we add definitions for aliases. An alias is a placeholder for a set of instructions, which are to executed only on demand. Since, we add the alias definitions to the .bashrc file, the aliases we defined are available in every Terminal. However, in contrast to sourcing OpenFOAM permanently, the OpenFOAM environment is set up only when we invoke the alias. An alias is a conventient way to save on typing effort, since we can assign one or several commands of arbitrary length15 to a rather short alias. We are free to choose the alias’ name, as long as this name does not collide with an existing command16 .

In Listing 21 two aliases are shown for enabling OpenFOAM-3.0 and OpenFOAM-4.1. If we want to use OpenFOAM-3.0, we simply type of30 into the Terminal, this will source the environment for OpenFOAM-3.0. The use of these four letter aliases, which include the major and minor version number of OpenFOAM, saved us from typing a 46 character command to enable the OpenFOAM environment.
```
alias of30 =’ source $HOME / OpenFOAM / OpenFOAM -3.0/ etc / bashrc ’
alias of41 =’ source $HOME / OpenFOAM / OpenFOAM -4.1/ etc / bashrc ’
```
Listing 21: Sourcing OpenFOAM on demand by using an alias

#### 8.2 Useful helpers
